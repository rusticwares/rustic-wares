Rustic Wares grew out of a passion for the outdoors and the desire to do something about protecting it. We hold on to the belief that everything can be saved or repurposed for the next generation to enjoy. We all need things to make a house a home, or to create a special environment to stir creativity at the office and nothing that you can buy new in a store tells a story like a reclaimed apothecary drawer or an old slab of wood. 

Wherever possible we used reclaimed, re-purposed, recycled or natural materials. Most of our wood for charcuterie boards and kitchen utensils are pieces of much bigger projects. By teaming up with the local lumber mill we gets the heads up on truly beautiful pieces that are too small for big projects - but perfect for RusticWares.

Website: https://www.rusticwares.com/
